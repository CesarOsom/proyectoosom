var gulp = require("gulp");
var webserver = require("gulp-webserver");
var compass = require("gulp-compass");

gulp.task("webserver", function() {
  gulp.src("app")
    .pipe(webserver({
      livereload: true,
      fallback: "index.html",
      directoryListing: {
        enable: true,
        path: "app"
      },
      open: true
    }));
});

gulp.task("compass", function(){
  gulp.src("./sass/**/*.scss")
    .pipe(compass({
       generated_images_path: 'app/css',
       sprite_load_path: ['src/sprites'],
       images_dir: 'src/sprites',
       image: 'src/sprites',
      css: "app/css",
      sass: "sass",
    }))
    .pipe(gulp.dest("app/css"));
});

gulp.task("compass:watch", function(){
  gulp.watch("./sass/**/*.scss",["compass"]);
});
